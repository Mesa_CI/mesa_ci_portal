function state(elm) {
  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }   

  $.ajax({
    type: 'POST',
    url: 'state',
    contentType: 'application/json',
    data: {
      name: elm.name,
      text: elm.innerText
    },
      success: async function (result) {
          if (result.status == 'OK') {
            let outlet = elm.parentElement.attributes.class.value;
            if (outlet.includes('off')) {
              classTag = 'off'
            } else if (outlet.includes('on')) {
              classTag = 'run'
            } else if (outlet.includes('cycle')){
              classTag = 'cycle'
            } else if (outlet.includes('run')){
              classTag = 'run'
            }

            if (result.state == 'off') {
              outlet = outlet.replace(classTag, 'off');
              elm.parentElement.attributes.class.value = outlet
            } else if (result.state == 'on') {
              outlet = outlet.replace(classTag, 'run');
              elm.parentElement.attributes.class.value = outlet
            } else if (result.state == 'cycle') {
              outlet = outlet.replace(classTag, 'cycle');
              elm.parentElement.attributes.class.value = outlet
              await sleep(30000);
              elm.parentElement.attributes.class.value = outlet.replace('cycle', 'run');
            } else {
              outlet = outlet.replace(classTag, 'run');
              elm.parentElement.attributes.class.value = outlet
            }
          } else {
            console.log('-- else STATUS --', result);
          }
      },
      error: function (error) {
          console.log(error);
      }
  });
};


function get_pdu_info(host) {
  $.ajax({
    type: 'POST',
    url: 'get_pdu_info',
    contentType: 'application/json',
    data: {
      host: host,
    },
      success: function (result) {
          if (result.status == 'OK') {
            for (let i = 1; i <= 8; i++) {
              let pdu_num = document.getElementById(i).className;
              let outlet_state = 'none';
              if (pdu_num.includes('none')) {
                classTag = 'none';
              } else if (pdu_num.includes('off')) {
                classTag = 'off';
              } else if (pdu_num.includes('on')) {
                classTag = 'run';
              } else if (pdu_num.includes('cycle')){
                classTag = 'cycle';
              } else if (pdu_num.includes('run')){
                classTag = 'run';
              }

              if (result[i] == true) {
                outlet_state = 'run';
              } else if (result[i] == false) {
                outlet_state = 'off';
              }

              if (document.getElementById(i).firstElementChild.nextElementSibling.innerText != 'None') {
                pdu_num = pdu_num.replace(classTag, outlet_state);
                document.getElementById(i).className = pdu_num
              }

            }
          } else if (result.status == 'error') {
            console.log('-- Status: Error PDU ' + host + ' RESULT --', result);

          } else {
            console.log('--else PDU ' + host + ' RESULT --', result);
          }
      },
      error: function (error) {
          console.log('-- ERROR PDU: ' + host + ' --', error);
      }
  });
};
