#!/usr/bin/env python3

# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    ECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>


from flask import request, render_template
from core.main import bp as pdu

import core.utils.pdu_subsystems as ps


@pdu.route('/<int:pdu>')
def single_pdu(pdu):
    """
    Renders Single PDU page to User.
    """
    try:
        conf = ps.get_system(pdu)
    except UnboundLocalError:
        conf = ['Error', 'Error', 'Error', 'Error',
                'Error', 'Error', 'Error', 'Error', 
                'Error', 'Error', 'Does not exist!']

    except Exception:
        conf = ['Error', 'Error', 'Error', 'Error',
                'Error', 'Error', 'Error', 'Error',
                'Error', 'Error']
    return render_template('main/single_pdu.html',conf=conf, pdu=pdu)


@pdu.route('/single_<int:pdu>')
def indexed_pdu(pdu):
    """
    Renders single PDU page to User
    """
    try:
        conf = ps.get_system(pdu)
    except UnboundLocalError:
        conf = ['Error', 'Error', 'Error', 'Error',
                'Error', 'Error', 'Error', 'Error',
                'Error', 'Error', 'Does not exist!']
    except Exception:
        conf = ['Error', 'Error', 'Error', 'Error',
                'Error', 'Error', 'Error', 'Error',
                'Error', 'Error']
    return render_template('main/single.html',conf=conf, pdu=pdu)


@pdu.route('/')
def index():
    """
    Renders Main page.
    """
    conf = ps.get_systems()
    return render_template('main/index.html', conf=conf)


@pdu.route('/state', methods=['POST','GET'])
def state():
    """
    Renders State of the pdu
    """
    data = request.data.decode('utf-8').split('&')
    outlet_name = data[0].split('=')[-1]
    outlet_state = data[1].split('=')[-1].lower()
    state = ps.set_outlet_state(outlet_name, outlet_state)

    if type(state) is dict:
        print(f"ERRORED RESPONSE: {state}")
        return state
    response = {'status': 'OK', 'name': outlet_name, 'state': outlet_state}

    return response, 200


@pdu.route('/get_pdu_info', methods=['POST','GET'])
def get_pdu_info():
    """
    Renders PDU Info
    """
    pdu_info = []
    pdu_num = request.data.decode('utf-8').split('=')[-1]
    host_ip = ps.get_system(pdu_num)[1]
    pdu_info = ps.get_pdu_info(host_ip)

    return pdu_info, 200
