#!/usr/bin/env python3

# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>


import json
import requests
from flask import jsonify, make_response

timeoutx = 20


def get_pdu_login(host_ip, pdu_config='reboot_hung_systems_config.json'):
    with open(pdu_config, encoding="utf8") as Config:
        data = json.load(Config)
        user, passwd = None, None

        for _, pdu_info in data['switches'].items():
            if host_ip == pdu_info['hostname']:
                user, passwd = pdu_info['userid'], pdu_info['password']

        return [user, passwd]


def get_systems(pdu_config='reboot_hung_systems_config.json'):
    """Get PDU Config details from Master CI"""
    systems = []

    with open(pdu_config, encoding="utf8") as Config:
        data = json.load(Config)

        for pdu_num, pdu_info in data['switches'].items():
            pdu = [pdu_num, pdu_info['hostname']]
            outlet_set = [None, None, None, None, None, None, None, None]
            for host, host_info in data['systems'].items():
                if int(host_info['switch']) == int(pdu_num):
                    for outlet in range(8):
                        if outlet+1 == int(host_info['outlet']):
                            outlet_set[outlet] = host.replace('.local', '')
            pdu.extend(outlet_set)
            systems.append(pdu)
    return systems 


def get_system(req_pdu_num, pdu_config='reboot_hung_systems_config.json'):
    """Get Single PDU Config details from Master CI"""
    pdu = [None]
    with open(pdu_config, encoding="utf8") as Config:
        data = json.load(Config)
        for pdu_num, pdu_info in data['switches'].items():
            if int(pdu_num) == int(req_pdu_num):
                pdu = [pdu_num, pdu_info['hostname']]
                outlet_set = [None, None, None, None, None, None, None, None]
                for host, host_info in data['systems'].items():
                    if int(host_info['switch']) == int(pdu_num):
                        for outlet in range(8):
                            if outlet+1 == int(host_info['outlet']):
                                outlet_set[outlet] = host.replace('.local', '')
                pdu.extend(outlet_set)
    # print(f"get_system >>: {pdu}")
    return pdu 


def get_outlet_by_name(outlet_name, pdu_config='reboot_hung_systems_config.json'):
    with open(pdu_config, encoding="utf8") as Config:
        data = json.load(Config)
        for conf_outlet_name, conf_info in data['systems'].items():
            if conf_outlet_name.replace('.local', '') == outlet_name:
                switch_num = conf_info['switch']
                outlet_location = conf_info['outlet'] - 1

        for switch, switch_info in data['switches'].items():
            if int(switch) == switch_num:
                host_ip = switch_info['hostname']
                username = switch_info['userid']
                password = switch_info['password']

    return outlet_location, host_ip, username, password


def get_outlet_states(pdu_ip, username, password, outlet='0,1,2,3,4,5,6,7'):
    '''
    This will get status of the each outlet if its on or off
    Passed variable to this function must always have outlet number subtructed 
    due to current config is 1-8 while pdu is 0-7
    ---
    Both valid call for the outlet: 
    int for single outlet
    string for multiple outlets
    ---
    get_outlet_states('192.168.1.8', 'user', 'password', 1)
    get_outlet_states('192.168.1.8', 'user', 'password', '1,5')
    '''
    outlet_states = {}
    outlet = outlet.replace(' ', '')
    uri = f'http://{username}:{password}@{pdu_ip}/restapi/relay/outlets/={outlet}/state/'

    try:
        response = requests.get(uri, timeout=timeoutx)
    except requests.ConnectionError as err:
       return { 'status': 'error', 'error': 'Connection Error', 'description': err }
    except Exception as err:
       return { 'status': 'error', 'error': 'Connection Timeout', 'description': err}
    
    json_parse = response.text
    print(f" JSON PARSE: {json_parse}")

    if '<META HTTP-EQUIV="refresh" content="0; URL=/index.htm">' in json_parse:
        return { 'status': 'error', 'error': 'Unsuported PDU' }

    if "Security lockout in place" in json_parse:
        return { 'status': 'error', 'error': 'Security lockout in place' }
    
    data = json.loads(json_parse)
    outlet = outlet.split(',')

    for index, item in enumerate(outlet):
        outlet_states[str(int(item) + 1)] = data[index]

    return outlet_states


def set_outlet_state(outlet_name, outlet_state):
    outlet_location, host_ip, username, password = get_outlet_by_name(outlet_name)
    try:
        if outlet_state == 'on':
            headers = {"X-CSRF": "x"}
            payload = {"value": "true"}
            state_uri = f'http://{username}:{password}@{host_ip}/restapi/relay/outlets/{outlet_location}/state/'
            response = requests.put(state_uri, data=payload, headers=headers, timeout=timeoutx)
        elif outlet_state == 'off':
            headers = {"X-CSRF": "x"}
            payload = {"value": "false"}
            state_uri = f'http://{username}:{password}@{host_ip}/restapi/relay/outlets/{outlet_location}/state/'
            response = requests.put(state_uri, data=payload, headers=headers, timeout=timeoutx)
        elif outlet_state == 'cycle':
            headers = {"X-CSRF": "x"}
            state_uri = f'http://{username}:{password}@{host_ip}/restapi/relay/outlets/{outlet_location}/cycle/'
            response = requests.post(state_uri, headers=headers, timeout=timeoutx)
        else:
            state_uri = None
            response = None
    except:
        return { 'status': 'error', 'error': 'Connection Aborted', 'description': 'Check your PDU, if it supports RestApi' }
    return response


def get_pdu_config(pdu_ip, username, password):
    ''' 
    This Function will return the following
    1. PDU Name
    2. PDU Version
    3. Timezone
    4. en/dis status of the  rest api
    5. en/dis status of the ssh
    6. list of available ssh .pub keys
    '''
    uri = f'http://{username}:{password}@{pdu_ip}/restapi/config/'

    try:
        response = requests.get(uri, timeout=timeoutx)
    except requests.ConnectionError:
        return { 'status': 'error', 'error': 'Connection Error' }
    except requests.exceptions.ReadTimeout:
        return { 'status': 'error', 'error':'Connection Timeout' }
    except Exception:
       return { 'status': 'error', 'error': 'Unknown Error' }
    
    json_parse = response.text

    try:
        data = json.loads(json_parse)
    except Exception:
        return {'status': 'error', 'error': 'Unsupported PDU' }

    try:
        if data['error']:
            return data
        else:
            return 'junk'
    except:
        return data['hostname'], data['version'], \
           data['timezone'], data['allow_restapi'], \
           data['ssh_enabled'], data['ssh_authorized_keys']


def get_pdu_info(pdu_ip):
    def Merge(d1, d2):
        out_dict = d1.copy()
        out_dict.update(d2)
        return out_dict

    username, password = get_pdu_login(pdu_ip)
    info = get_pdu_config(pdu_ip, username, password)

    if type(info) is dict:
        response = info
    else:
        response = {'status' : 'OK', 'hostname': info[0], 'version': info[1], 
                'timezone': info[2], 'restapi': info[3],
                'ssh': info[4], 'ssh_key': info[5] }
  
  
    outlet_states = get_outlet_states(pdu_ip, username, password)

    # print(f"outlet_states Type = {type(outlet_states)}" )
    # print(f"Trying Outlet States: {outlet_states}")
    if type(outlet_states) is tuple:
        # print(f"outlet_states: {outlet_states}")
        return outlet_states
    results = Merge(response, outlet_states)


    # if outlet_states["status"]:
    #     print("----------------------", outlet_states)

    # results = response | outlet_states

    
    # print(f"results: {results}")


    return results







