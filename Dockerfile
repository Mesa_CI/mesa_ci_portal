FROM python:alpine

ENV PYTHONDONTWRITEBYTECODE 1 \
    PYTHONUNBUFFERED 1

WORKDIR /app

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add --no-cache mariadb-dev

RUN pip install --upgrade pip
RUN pip install --no-cache-dir \
    flask \
    flask-migrate \
    flask-modals \
    flask-sqlalchemy \
    gunicorn \
    mysqlclient \
    psycopg2-binary \
    pydantic \
    pymysql \
    python-decouple \
    requests

COPY ./app ./app

CMD [ "python", "-u", "main.py" ]
