### New install must run below command to init database
Run below commands to initiate the database after ```docker-compose up -d``` is ran

# flask_web
```bash
docker exec pdu_web flask db init
docker exec pdu_web flask db migrate
docker exec pdu_web flask db upgrade
```


This is a basic server setup with a proxy as frontend and rest of the server setup behind
keep in mind this is work in progress 